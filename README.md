# **Inmersion en Datos con Python**

- Videos
  - [**Clase N°1**](https://drive.google.com/file/d/1sEk2zYM5Xn45cxY2jJRWdvjEza1LagY7/view?usp=sharing)
  - [**Clase N°2**](https://drive.google.com/file/d/1gKeQAFG2UE8KnIhYxFk_AWkT_wf-SSr3/view?usp=sharing)
  - [**Clase N°3**](https://drive.google.com/file/d/1TvPSQ9nnNR26b_euO3SeO8zXPaMgX_RJ/view?usp=share_link)
  - [**Clase N°4**](https://drive.google.com/file/d/1Uk2yXTWZfBeIoEDxWlvObj_wqP9p1zlC/view?usp=sharing)
- Contenido
  - **Clase N°1**
    - **Importar datos**
    - **Ver dimension** de los datos
    - Ver los atributos en lista
    - **Renombrar** columnas
    - Obtener informacion de los atributos/columnas
    - **Seleccionar** uno o mas registros **por posición**
    - Saber el **tipo de datos**
    - Obtener la media de un atributo/columna
    - **Seleccionar** n registros **al azar**
    - Obtener un dato Series con booleanos a partir de un condicional
    - Funncion **sum**
    - Funcion **len**
    - **Contar** cantidad de veces de un atributo
    - Ver datos con **grafico de barras**
  - **Clase N°2**
    - Separar por espacio ' '
    - Separar y **convertir en formato DataFrame**
    - Obtener una parte de nuestro DataFrame
    - **Reemplazar** caracter por otro en una columna
    - Muestra una estadistica rapida de todos los atributos del tipo numerico
    - Ver historico de una variable
    - **Histograma** con **Matplotlib** y **Seaborn**
  - **Clase N°3**
    - Crear un atributo a traves de una operacion de otros atributos
    - **Agrupar** por atributo barrio
    - Crear un diccionario para un atributo dado
    - **Sustituir valores** a traves de un diccionario y map
    - **Obtener los indices**(en este caso'Barrio') de los 10 barrios con mas inmuebles (values_counts)
    - Crea un indice numerico y el indice que es 'Barrio' pasa a ser un atributo mas
    - Obtener los primeros 10 barrios con mas inmuebles a traves de una **query** @<name_indices>
    - Uso de grafico de barra (**barplot**)
    - Uso de grafico de box (**boxplot**)
    - Uso de **grafico de box (boxplot) y agregando filtro**
    - **Mergear** dos "tablas" a traves de un atributo en comun (JOIN)
  - **Clase N°4**
    - Agrupar por atributo y obtener media de un conjunto dado
    - Seleccion por medio de un **query**
    - Uso de grafico **scatterplot**
    - Ver **correlaciones** entre variables
    - Ver grafico de mapa de correlaciones con **heatmap**
    - **Machine Learning** : Creacion de un modelo de clasificacion simple con LinearRegression y train_test_split

---

## **Clase N°1**

### **Cargar nuestros datos**

```py
from google.colab import drive
drive.mount('/content/drive')
```

### **Saber la dimension de mis datos**

```py
inmuebles.shape
```

### **Obtener la lista con los nombres de las columnas**

```py
inmuebles.columns
```

### **Renombrar columnas**

```py
columnas = {'Baños':'Banos','Área':'Area'}
inmuebles = inmuebles.rename(columns=columnas)
inmuebles.sample(10)
```

### **Obtener informacion de mis datos**

```py
inmuebles.info()
```

### **Indexación puramente basada en la ubicación de enteros para la selección por posición**

```py
inmuebles.iloc[300]
inmuebles.iloc[300:305]
inmuebles['Valor'][300]
```

### **Para saber el tipo de dato**

```py
type(inmuebles['Valor'][300:305])
```

### **Para calcular la media de una serie de datos**

```py
inmuebles.<nombre_columna>.mean()
```

### **Obtener 100 registros al azar**

```py
inmuebles.sample(100)
```

### **Obtener un data.series booleando con un condicional dado**

```py
(inmuebles.Barrio == 'Chico Reservado')
```

### **Funcion suma**

```py
sum((inmuebles.Barrio == 'Chico Reservado'))
```

### **Devuelve el largo de lo que le pasamos**

```py
len(<nombre_dato>)
```

### **Cuenta la cantidad de veces que aparece cada registro del <nombre_columna>**

```py
inmuebles.<nombre_columna>.value_counts()
```

### **Visualizar los datos con barras**

```py
inmuebles_barrio = inmuebles.Barrio.value_counts()
inmuebles_barrio.head(10).plot.bar() # con .head(10) tenemos los primeros 10
```

**Desafío**

- 1.1 Promedio de área de todos los inmuebles en los barrios en el dataset. El top 10.
- 1.2 Consultar otros datos estadísticos, conteo, mediana, valores mínimo y máximo.

```py
# 1.1
barrios_agrupados = inmuebles.groupby(by="Barrio"); #Agrupamiento por barrio
barrios_media_area = barrios_agrupados.Area.mean(); #Media del area de cada barrio (agrupado)
barrios_media_area_ordenados = barrios_media_area.sort_values(ascending=False); #Ordenado de manera Descendiente
barrios_media_area_ordenados.head(10).plot.bar(); #Los primeros 10 y graficado
```

---

## **Clase N°2**

### **Separar ' ' por espacio**

```py
inmuebles.Valor[0].split()
```

### **Separar y convertir en formato DataFrame**

```py
inmuebles.Valor.str.split(expand=True)
```

### **Obtener una parte de nuestro DataFrame**

```py
inmuebles[['Precio','Barrio']]
```

### **Reemplazar caracter por otro en una columna**

```py
inmuebles['Precio'] = inmuebles['Precio'].str.replace('.','',regex=True)
```

### **Muestra una estadistica rapida de todos los atributos del tipo numerico**

```py
inmuebles.describe()
```

### **Ver historico de una variable**

```py
inmuebles['Precio_Millon'].plot.hist(bins=10) # - Bins + Concentrado las barras, + Bins - Concentrada las barras
```

### **Histograma con Matplotlib y Seaborn**

```py
import matplotlib.pyplot as plt
import seaborn as sns

plt.figure(figsize=(10,6))
grafica = sns.histplot(data=inmuebles, x='Precio_Millon', kde=True, hue='Tipo')
grafica.set_title('Distribución de Valores de los inmuebles en Bogotá')
plt.xlim((50,1000))
plt.savefig('/content/drive/MyDrive/Colab Notebooks/valor_inmuebles.png',format='png')
plt.show()
```

**Desafíos**

- 2.1 Estudiar mejor el histograma de valores, seleccionar 3 tipos de inmuebles (Refinar el gráfico: Títulos, aumentar el tamaño de labels, colores, conclusión de la información)
- 2.2 Precio del m2 por barrio y hacer el gráfico más adecuado para esta nueva variable.

```py
# 2.2
inmuebles['Valor_m2_Millon'] = inmuebles['Precio_Millon']/inmuebles['Area'] #Creacion del atributo precio en millon del metro cuadrado
barrios_agrupados = inmuebles.groupby(by="Barrio"); #Agrupamiento por barrio
barrios_media_valor_m2 = barrios_agrupados.Valor_m2_Millon.mean(); #Media del valor m2 de cada barrio (agrupado)
barrios_media_valor_m2_ordenados = barrios_media_valor_m2.sort_values(ascending=False); #Ordenado de manera Descendiente
barrios_media_valor_m2_ordenados.head(10).plot.bar(); #Los primeros 10 y graficado
```

---

## **Clase N°3**

### **Crear un atributo a traves de una operacion de otros atributos**

```py
inmuebles['Valor_m2_Millon'] = inmuebles['Precio_Millon']/inmuebles['Area']
```

### **Agrupar por atributo barrio**

```py
inmuebles.groupby('Barrio')
```

### **Crear un diccionario para un atributo dado**

```py
# Hace un diccionario con Key/Index_of_DataFrame como 'Barrio' y Value como 'Valor_m2_Barrio'
dict(datos_barrio['Valor_m2_Barrio'])
```

### **Sustituir valores a traves de un diccionario y map**

```py
m2_barrio = dict(datos_barrio['Valor_m2_Barrio'])
inmuebles['Valor_m2_Barrio'] = inmuebles['Barrio']
inmuebles['Valor_m2_Barrio'] = inmuebles['Valor_m2_Barrio'].map(m2_barrio)
```

### **Obtener los indices(en este caso'Barrio') de los 10 barrios con mas inmuebles (values_counts)**

```py
top_barrios = inmuebles['Barrio'].value_counts()[:10].index
```

### **Crea un indice numerico y el indice que es 'Barrio' pasa a ser un atributo mas**

```py
datos_barrio.reset_index(inplace=True)
```

### **Obtener los primeros 10 barrios con mas inmuebles a traves de una query @<name_indices>**

```py
datos_barrio.query('Barrio in @top_barrios')
```

### **Uso de grafico de barra (barplot)**

```py
plt.figure(figsize=(10,8))
ax = sns.barplot(x="Barrio", y="Valor_m2_Barrio", data = datos_barrio.query('Barrio in @top_barrios'))
ax.tick_params(axis='x', rotation=45) #agrega nombre a los parametros/barras
```

### **Uso de grafico de box (boxplot)**

```py
plt.figure(figsize=(10,8))
ax = sns.boxplot(x="Barrio", y="Valor_m2_Millon", data = inmuebles.query('Barrio in @top_barrios'))
ax.tick_params(axis='x', rotation=45)
plt.show()
```

### **Uso de grafico de box (boxplot) y agregando filtro**

```py
plt.figure(figsize=(10,8))
ax = sns.boxplot(x="Barrio", y="Valor_m2_Millon", data = inmuebles.query('Barrio in @top_barrios & Valor_m2_Millon < 15')) #filtro : Valor_m2_Millon < 15
ax.tick_params(axis='x', rotation=45)
plt.show()
```

### **Mergear dos "tablas" a traves de un atributo en comun (JOIN)**

```py
datos_dane = pd.merge(datos_a,datos_b,on='DIRECTORIO', how='left') #atributo:'DIRECTORIO', Tipo join: 'join left'
```

**Desafío**

- 3.1 Dar un vistazo a la base de datos del DANE, entender estas variables conceptualmente para entender mejor el contexto de esta base.
- 3.2 Organizar tu notebook para que tu proyecto quede mejor presentado.

---

## **Clase N°4**

### **Agrupar por atributo y obtener media de un conjunto dado**

```py
datos_dane.groupby('NOMBRE_ESTRATO')[['CONJUNTO_CERRADO','INSEGURIDAD','TERMINALES_BUS','BARES_DISCO','RUIDO','OSCURO_PELIGROSO','SALARIO_MES','TIENE_ESCRITURA','PERDIDA_TRABAJO_C19','PERDIDA_INGRESOS_C19','PLANES_ADQUIRIR_VIVIENDA']].mean().head()
```

### **Seleccion por medio de un query**

```py
datos_ml = datos_ml.query('Precio_Millon < 1200 & Precio_Millon > 60')
datos_ml
```

### **Uso de grafico scatterplot**

```py
plt.figure(figsize=(10,8))
sns.scatterplot(data=datos_ml, x='SALARIO_ANUAL_MI',y ='Valor_m2_Millon')
plt.ylim((0,15))
plt.show()
```

### **Ver correlaciones entre variables**

```py
datos_ml.corr()
```

### **Ver grafico de mapa de correlaciones con heatmap**

```py
plt.figure(figsize=(18, 8))
#https://www.tylervigen.com/spurious-correlations
#mascara = np.triu(np.ones_like(datos_ml.corr(), dtype=bool)) mask=mascara,
heatmap = sns.heatmap(datos_ml.corr(), vmin=-1, vmax=1, annot=True, cmap='BrBG')
heatmap.set_title('Correlación de las variables', fontdict={'fontsize':18}, pad=16);
```

### **Machine Learning : Creacion de un modelo de clasificacion simple con LinearRegression y train_test_split**

```py
...
```
